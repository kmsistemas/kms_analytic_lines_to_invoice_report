# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This module,
#    Copyright (C) 2011 KM Sistemas de Información, S.L. - http://www.kmsistemas.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': 'KM Sistemas - Informe de líneas analíticas',
    'version': '1.0',
    'category': 'Reporting',
    'description': """
    Este módulo muestra un informe Jasper con los datos seleccionados 
    de la vista de Contabilidad|Procesamiento periódico|Facturación|
    Facturar trabajos de tareas, mostrando las líneas analíticas
    seleccionadas.
    Los resultados se muestran agrupados por partner y ordenados
    cronológicamente. El módulo pasa, además, los datos del filtro
    aplicado en el dominio de la search view para que los criterios
    de rango de fechas y usuario aparezcan en la cabecera de grupo.
    Los textos no están traducidos.
    """,
    'author': 'KM Sistemas de información, S.L.',
    'depends': ['account','jasper_reports'],
    'update_xml': [
        'kms_analytic_lines_to_invoice_wizard.xml',
    ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'active': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
