# -*- coding: utf-8 -*-
###############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This module,
#    Copyright (C) 2011 KM Sistemas de Información, S.L. - http://www.kmsistemas.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import pooler
import wizard
import jasper_reports
from datetime import datetime
from osv import osv, fields

form='''<?xml version="1.0" encoding="utf-8"?>
<form string="Opciones">
	<separator string="Parámetros del informe" colspan="4"/>
	<field name="printlogo"/>
</form>'''

fields={
	'printlogo': {'string': 'Imprimir logo', 'type': 'boolean', 'help': 'Marque esta casilla si desea imprimir el logotipo de la compañía.'},
}

class wiz_analytic_lines_report(wizard.interface):
	def _init(self, cr, uid, data, context):
		data['form']['printlogo'] = True
		return data['form']

	def parser( cr, uid, ids, data, context ):
		
		# SUBTITLE Parameters
		subtitle = {}
		start_date = ''
		end_date = ''
		user = ''
			
		ids = []
		name = ''
		model = ''
		records = []
		data_source = 'model'
		parameters = {}

		domain = data['_domain']
		for domain_element in domain:
			if domain_element[0] == 'date' and domain_element[1] == '>=':
				start_date = domain_element[2]
			elif domain_element[0] == 'date' and domain_element[1] == '<=':
				end_date = domain_element[2]	
			elif domain_element[0] == 'user_id' and domain_element[1] == 'ilike':
				user = domain_element[2]
						
		if start_date:
		    subtitle['start_date'] = datetime.strptime( start_date, '%Y-%m-%d' ).strftime( '%d/%m/%Y' )
		else:
		    subtitle['start_date'] = '*'
		if end_date:
		    subtitle['end_date'] = datetime.strptime( end_date, '%Y-%m-%d' ).strftime( '%d/%m/%Y' )
		else:
		    subtitle['end_date'] = '*'
		
		s = ''
		#s += str(data['_domain'])
		
		if start_date and end_date:
			s += ( 'Fechas: %s - %s; ') % (subtitle['start_date'], subtitle['end_date'])
		if user:
			s += ' Usuario contiene "%s"; ' % user
		
		parameters['SUBTITLE'] = s
		
		
		
		parameters['imprimirlogo'] = str(data['form']['printlogo'])
		
		name = 'report.kms_analytic_lines_to_invoice_jasper'
		model = 'account.analytic.line'
		data_source = 'model'
		
		return { 
		    'ids': ids, 
		    'name': name, 
		    'model': model, 
		    #'records': records, 
		    #'data_source': data_source,
		    'parameters': parameters,
		}

	jasper_reports.report_jasper( 'report.kms_analytic_lines_to_invoice_jasper', 'account.analytic.line', parser )
	#jasper_reports.report_jasper('report.km_invoice_report.jasper', 'account.invoice', parser=_print)

	states={
		'init':{
			'actions': [_init],
			'result': {'type': 'form', 'arch': form, 'fields': fields, 'state': [('end', 'Cancel'), ('report','Print')]}
		},
		'report':{
			'actions': [parser],
			'result': {'type': 'print', 'report': 'report.kms_analytic_lines_to_invoice_jasper', 'state': 'end', 'rml': 'kms_analytic_lines_to_invoice_report/reports/Trabajo.jrxml'}
		}
	}
wiz_analytic_lines_report('kms_analytic_lines_to_invoice_report_printwizard')

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
